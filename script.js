/*****************************************************************************
                                PORTFOLIO SECTION
*****************************************************************************/
$(document).ready(function(){
    $("#isotope-container").isotope({});
    
    $("#isotope-filters").on("click","button",function(){
        let filterValue = $(this).attr("data-filter");
        $("#isotope-container").isotope({
           filter: filterValue 
        });
        
        $("#isotope-filters").find('.active').removeClass('active');
        $(this).addClass('active');
    });
});

$(document).ready(function(){
  $("#client-list").owlCarousel({
      items:6,
      autoplay:true,
      margin:20,
      loop:true,
      nav:false,
      smartSpeed:500,
      autoplayHoverPause:true,
      dots:false,
      navText: ['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i>']
  });
});
